from functions import simulation, paranoia_eq_prior
import networkx as nx
import seaborn as sns
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

seeds = range(100)
prior0 = (11, 6)
results = []
cog_dis = 0

param = 4
#G = nx.grid_2d_graph(3, 3)
number_of_agents = 100
G = nx.erdos_renyi_graph(number_of_agents, 0.4)
adj = nx.adj_matrix(G).todense().astype(bool)
justice = 0.3
payoff = np.array([[1, 0], [1.1, 0.1]])
payoff += np.eye(2) * justice
tmax = 100

for prior in [(11., 6.), (13., 4.)]:
    default_prior = np.array([[[prior[0]]], [[prior[1]]]])
    paranoia = paranoia_eq_prior(prior, prior0)
    for seed in seeds:
        result = simulation(seed, number_of_agents, tmax, default_prior,
                            paranoia, cog_dis, payoff, adj)
        results.extend(result)

df = pd.DataFrame(results)
sns.tsplot(df, time='Time', unit='seed', value='Fraction of Corrupted Edges',
           condition='Prior')
plt.show()
