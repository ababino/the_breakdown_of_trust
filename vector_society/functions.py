"""Functions used in percolation.py.

The main function is simulation which runs act and observe.
"""
from __future__ import division
import numpy as np
import networkx as nx
from scipy.special import btdtr, btdtri
from scipy import stats, sparse
import random
from IPython import embed


def paranoia_eq_prior(prior1, prior2):
    """Calculate paranoia.

    For two priors prior1 and prior2 calculates the paranoia that needs to be
    added to prior1 in order to achive the same defection stimation that as
    prior2
    """
    beta = stats.beta(prior1[1], prior1[0])
    mean2 = prior2[1] / sum(prior2)
    paranoia_th = beta.cdf(mean2) - beta.cdf(beta.mean())
    return paranoia_th


def act(payoff_threshold, src, src_is_ald, beta_lut, pd, own_prior_sum,
        degrees):
    """Action stage."""
    o = (degrees + own_prior_sum*beta_lut.shape[2]).take(src)
    o += pd*(beta_lut.shape[1]*beta_lut.shape[2])
    # Estimated probability that the other chooses to defect.
    p = np.squeeze(beta_lut.take(o))
    # action is True if the agent chooses to defections
    action = p > payoff_threshold
    action[src_is_ald] = True
    return np.asarray(action)


def observe(action, prior, own_prior, src, trs, pd, own_prior_sum, t,
            number_of_agents):
    """Observation stage."""
    # index where the value to replace is. We keep the extremes to avoid
    # improper priors
    i = t % (prior.shape[0] - 2) + 2
    pd -= prior[i, :]
    prior[i, :] = action.take(trs)
    pd += prior[i, :]

    # index where the value to replace is
    i = t % own_prior.shape[0]
    # substract the old value
    own_prior_sum -= own_prior[i, :]
    # compute and store the new value
    own_prior[i, :] = np.bincount(src[action], minlength=number_of_agents)
    # add the new value
    own_prior_sum += own_prior[i, :]
    return prior, own_prior, pd, own_prior_sum


def simulation(seed, number_of_agents, tmax, default_prior, paranoia, cog_dis,
               payoff, adj, ald, beta_lut):
    """Main loop.

    This is the main loop of the simulation. The information is stored in numpy
    arrays which first dimension is the ineraction one. The src array cointains
    the sources of the interactions and dst the destinies. The trs tanform the
    interaction dimension in the transposed interaction.
    """
    # Set the seed
    np.random.seed(seed=seed)
    random.seed(seed)

    # In order to make the code memory efficient we use a list of source,
    # agents and a list of destinies agents, instead of a sparse matrix.
    # Note that the src list repeats each agent as many times as links it has.
    # I think of this like the agents/nodes space and the links/actions space
    (src, dst, vals) = sparse.find(adj)
    # Degrees of each agent
    degrees = adj.sum(0)
    # trs (transpose) is the order that we have to use if we want to...
    # trs = np.argsort(dst + src / number_of_agents)
    trs = np.argsort(dst + src / number_of_agents)

    # The sparse module change its find definition output (compare v0.17 and
    # v0.19)
    if not ((src[trs] == dst).all() and (src == dst[trs]).all()):
        trs = np.argsort(src + dst / number_of_agents)
        if not ((src[trs] == dst).all() and (src == dst[trs]).all()):
            raise Exception('There is a problem with the transpose definition')

    # The prior matrix is a size=(memory, number_of_links) that contains the
    # last memory (default 12) actions of the other agents. Each column
    # corresponds to a link src-dst and stores what agent src expirienced from
    # agent dst. In each row there is a 1 if the action expirenced was a
    # defection and a 0 if the action expirienced was a cooperation.
    prior = np.vstack([np.zeros((default_prior[0]-1, adj.sum())),
                       np.ones((default_prior[1]-1, adj.sum()))])

    # This code randomizes the events in prior for each agent.
    ix_i = np.random.sample(prior.shape).argsort(axis=0)
    ix_j = np.tile(np.arange(prior.shape[1]), (prior.shape[0], 1))
    prior = prior[ix_i, ix_j]
    prior = np.vstack([np.zeros((1, adj.sum())), np.ones((1, adj.sum())),
                       prior])

    # pd is the sum of prior, that is the non biased estimated probablity of
    # defection of the other agent.
    pd = prior.sum(axis=0)

    # own_prior is the prior that an agent has of it self.
    own_prior = np.zeros((sum(default_prior), number_of_agents),
                         dtype=np.uint16)
    own_prior_sum = own_prior.sum(0, dtype=np.uint16)

    # is_ald is an array with the position of the AllD agents in the node space
    # and src_is_ald has the same information but in the action space.
    is_ald = ~np.ones(number_of_agents, 'bool')
    is_ald[ald] = True
    src_is_ald = is_ald[src]

    # payoff_threshold is the threshold up to which the optimum strategy is to
    # cooperate.
    p00 = payoff[0, 0]
    p01 = payoff[0, 1]
    p10 = payoff[1, 0]
    p11 = payoff[1, 1]
    payoff_threshold = (1e-15 - p10 + p00)/(p00 + p01 - p10 + p11)

    # result is a list of dics that will contain the output of each iteration.
    results = []
    result = {}
    result['seed'] = seed
    result['Proyection'] = cog_dis
    result['Paranoia'] = paranoia
    result['Prior'] = '({}, {})'.format(*default_prior)
    result['Parameters'] = 'Beta{}, Paranoia {}'.format(result['Prior'],
                                                        round(paranoia, 2))
    result['AlD'] = len(ald)

    # We reduce the type of the integers to the minimum possible in order to
    # increase efficiency.
    # own_prior will be used to update own_prior_sum. own_prior_sum and pd will
    # be used  will be use to index beta_lut, then they should have a type with
    # a maximum value greater then the len(bet_lut.flatten())
    best_type = None
    for int_type in [np.uint32, np.uint16, np.uint8]:
        if len(beta_lut.flatten()) <= np.iinfo(int_type).max:
            best_type = int_type
    if best_type is not None:
        own_prior = own_prior.astype(best_type)
        own_prior_sum = own_prior_sum.astype(best_type)
        prior = prior.astype(best_type)
        pd = pd.astype(best_type)
        degrees = degrees.astype(best_type)
    else:
        exc_str = "There is no int high enough. Probably, beta_lut is too big."
        raise Exception(exc_str)

    if src.max() < np.iinfo(np.uint32).max:
        src = src.astype(np.uint32)
        dst = dst.astype(np.uint32)
    if trs.max() < np.iinfo(np.uint32).max:
        trs = trs.astype(np.int32)
    end_sim = False
    for t in range(tmax):
        if not end_sim:
            action = act(payoff_threshold, src, src_is_ald, beta_lut, pd,
                         own_prior_sum, degrees)
            prior, own_prior, pd, own_prior_sum = observe(action, prior,
                                                          own_prior, src, trs,
                                                          pd, own_prior_sum, t,
                                                          number_of_agents)
            if (pd == 0).any():
                embed()
            defections = action.sum() / len(action)
            cooperations = 1 - defections
            result['Cooperations'] = cooperations
            result['Defections'] = defections
        result['Time'] = t
        results.append(result.copy())
        if t % 10 == 0:
            old_prior = prior.copy()
        if t % 10 == 1:
            if (old_prior == prior).all():
                end_sim = True
    sgc, sa = gigant_component_size(action, adj, src, dst, ald)
    return results, sgc, sa


def gigant_component_size(action, adj, src, dst, ald):
    # copy adj
    adj2 = adj.copy()
    # keep only the edges with a defection in any direction
    adj2[src[~action], dst[~action]] = 0
    # remove the edges with mixed actions (one defection and one cooperation)
    adj2 = adj2.multiply(adj2.T)
    # get te graph of mutual defetors
    G = nx.from_scipy_sparse_matrix(adj2)
    # get the components (and change set to list depnending on the nx version)
    comp = [list(nodes) for nodes in nx.connected_components(G)]
    # remove connected components with only one node that is not a ald
    # (those are not active)
    comp = [nodes for nodes in comp if len(nodes) > 1 or nodes in ald]
    # get the biggest component
    if len(comp) > 0:
        sgc = len(max(comp, key=len))
    else:
        sgc = 0
    # return the size of the biggest component
    return sgc, len(sum(comp, []))


def simulation_toy_model(seed, number_of_agents, tmax, default_prior, paranoia,
                         cog_dis, payoff, adj, ald, beta_lut):
    """Toy model.

    This is the main loop of the simulation. The information is stored in numpy
    arrays which first dimension is the ineraction one. The src array cointains
    the sources of the interactions and dst the destinies. The trs tanform the
    interaction dimension in the transposed interaction.
    """
    results = []
    np.random.seed(seed=seed)
    random.seed(seed)
    (src, dst, vals) = sparse.find(adj)
    degrees = np.array(adj.sum(0))
    degrees = np.squeeze(degrees)
    trs = np.argsort(dst + src / number_of_agents)
    if len(default_prior) > 2:
        prior = []
        for s in range(adj.shape[0]):
            p = adj[s, :].sum()
            prior.append(np.vstack([np.zeros((default_prior[s][0]-1, p)),
                                    np.ones((default_prior[s][1]-1, p))]))
        prior = np.hstack(prior)
    else:
        prior = np.vstack([np.zeros((default_prior[0]-1, adj.sum())),
                           np.ones((default_prior[1]-1, adj.sum()))])
    map(np.random.shuffle, prior.T)
    prior = np.vstack([np.zeros((1, adj.sum())), np.ones((1, adj.sum())),
                       prior])
    prior = prior.astype(int)
    if len(default_prior) > 2:
        own_prior = np.zeros((sum(default_prior[0]), number_of_agents),
                             dtype=int)
    else:
        own_prior = np.zeros((sum(default_prior), number_of_agents), dtype=int)
    own_prior_sum = own_prior.sum(0)
    is_ald = ~np.ones(number_of_agents, 'bool')
    is_ald[ald] = True
    src_is_ald = is_ald[src]
    result = {}
    result['seed'] = seed
    result['Proyection'] = cog_dis
    result['Paranoia'] = paranoia
    if len(default_prior) == 2:
        result['Prior'] = '({}, {})'.format(*default_prior)
        result['Parameters'] = 'Beta{}, Paranoia {}'.format(result['Prior'],
                                                            round(paranoia, 2))

    result['AlD'] = len(ald)
    result['Fraction of Corrupted Edges'] = 0
    results.append(result.copy())
    pd = prior.sum(axis=0)
    actions = []
    priors = []
    priors.append({(s, d): sum(p) for p, s, d in zip(prior.T, src, dst)})
    actions.append({(s, d): np.nan for s, d in zip(src, dst)})
    for i, ownp in enumerate(own_prior_sum):
        priors[-1][(i, i)] = ownp
    for t in range(tmax):
        action, is_coop = act(payoff, src, src_is_ald, beta_lut, pd,
                              own_prior_sum, degrees)
        prior, own_prior, pd, own_prior_sum = observe(action, prior, own_prior,
                                                      src, trs, pd,
                                                      own_prior_sum, t,
                                                      number_of_agents)
        cooperations = (action == 0).sum() / len(action)
        defections = action.sum() / len(action)
        fce = (~is_coop).sum() / len(action)
        result['Cooperations'] = cooperations
        result['Defections'] = defections
        result['Time'] = t
        result['Fraction of Corrupted Edges'] = fce
        results.append(result.copy())
        actions.append({(s, d): a for a, s, d in zip(action, src, dst)})
        priors.append({(s, d): sum(p) for p, s, d in zip(prior.T, src, dst)})
        for i, (ownp, d) in enumerate(zip(own_prior_sum, degrees)):
            priors[-1][(i, i)] = ownp / d
    return results, actions, priors


def build_network(dist, network_seed, param, expected_nodes=100):
    """Build networks with different degree distribution."""
    if dist == 'watts_strogats':
        G = nx.watts_strogatz_graph(expected_nodes, 4, param,
                                    seed=network_seed)
        return G
    random.seed(network_seed)
    number_of_nodes = 0
    while number_of_nodes < 0.9*float(expected_nodes):
        number_of_nodes = expected_nodes + 10
        if dist == 'exponential':
            sfun = np.random.exponential
            kwds = {'scale': param}
            deglist = nx.create_degree_sequence(number_of_nodes,
                                                sfunction=sfun,
                                                max_tries=500000, **kwds)
            G = nx.configuration_model(deglist, create_using=nx.Graph(),
                                       seed=None)

        elif dist == 'powerlaw':
            sfun = nx.utils.random_sequence.powerlaw_sequence
            kwds = {'exponent': param}
            deglist = nx.create_degree_sequence(number_of_nodes,
                                                sfunction=sfun,
                                                max_tries=500000,
                                                **kwds)
            G = nx.configuration_model(deglist, create_using=nx.Graph(),
                                       seed=None)
        elif dist == 'random':
            G = nx.erdos_renyi_graph(expected_nodes, param / expected_nodes)
        else:
            msg = 'dist should be exponential, powerlaw or random'
            print(dist, msg)
        G.remove_edges_from(G.selfloop_edges())
        G = sorted(nx.connected_component_subgraphs(G), key=len)[-1]
        number_of_nodes = len(G)
    return nx.convert_node_labels_to_integers(G)


def build_beta_lookuptable(n, paranoia, cog_dis, max_k):
    """It builds a manipulated_mean lookuptable in order to improve performance.

    It builds a manipulated_mean lookuptable in order to improve performance.
    n is the memory length of the agents and max_k is the maximun degree fo the
    network.

    It returns a matrix. Its rows are the defections of othres and its columns
    are its own defections.

    own_k is the degree of the agent in the node. This is necesary to normalize
    the output.
    """
    manipulated_mean = np.nan * np.ones((n + 1, max_k * (n + 1), max_k + 1))
    a = np.arange(0, n + 1, dtype='float')
    b = n - a
    for own_prior in xrange(max_k * (n + 1)):
        for own_k in xrange(max_k + 1):
            a2 = a.copy()
            b2 = b.copy()
            if own_k != 0:
                a2 += cog_dis * own_prior / own_k
                b2 += cog_dis * (n * own_k - own_prior) / own_k
            new_events = a2 + b2
            with np.errstate(invalid='ignore'):
                a2 = a2 * n / new_events
                b2 = b2 * n / new_events
            mean = a2 / (a2 + b2)
            proba = btdtr(a2, b2, mean)
            proba += paranoia
            out = btdtri(a2, b2, proba)
            manipulated_mean[:, own_prior, own_k] = out
    return manipulated_mean
