#The Breakdown of Trust
##Antecedentes

###Conocidos:
1. Las creencias de A sobre B predisponen acciones de A en B.
2. Juego de Confianza iterado: Uno aprende si confiar o no en el otro (Reputación social)
Las crencias sobre B se generan a partir de las acciones de B.

###De convienelty upset salen dos consideraciones más.

3. Conveniently Upset: Las acciones de A en B generan creencias de A en B.
4.  La ambiguedad en las creencias sobre B generan acciones mas nocivas.

¿Cual es el costo de (2)?

###Pheps
Otro elemento conocido es el paper de Phelps sobre update de juego de confianza en funcion de priors morales del otro.

Cada nodo hace inferncia bayesiana con lo que tiene de acuerdo al modelo (acciones propias, la visibilidad q tenga de accciones ajenas, q es algo a explorar).

La visibilidad puede ser un parametro muy importante.

Cada nodo vive en 2D. Los nodos tienen lugar y están distribuidos para que haya algo de clustering (hay barrios espaciales).

Cada nodo tiene una probabilidad de interactuar P(n)

Cada nodo interactua con un vecino cada tanto con probabilidad p(dist) =  gammaint^-d
y cada nodo conoce las acciones del vecino con probabilidad p(dist)= gammavisib^-d

y hace un update de acuerdo a distintos modelos que pueden ser

1. prediction error bayesiano sin prior
2. prediction error con mucha inercia si hay tags morales (phelps) bayesiano con prior
3. prediction errror + CU
4. 2+ CU

Idea: Reputación social genera soluciones biestables (todos confian o nadie) los priors morales generan cierta inmunidad q resiste a algunos detractores, conviniently uspet quizas juega al reves.

Una manera de pensarlo es sociedad en la que hay trust and how it breaks down.

The break down of trust.
