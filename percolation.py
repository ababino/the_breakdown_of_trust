"""main scrip.

You can call this script from command line like this:
>> python percolation.py 0.5 9 --ald 0 --expected_nodes 1000
or importing it into a python interactive session
>> from percolation import breakdown_trust
>> breakdown_trust(0, 8, 10000, 0, 0)
"""

from __future__ import division
from vector_society.functions import paranoia_eq_prior, build_beta_lookuptable
from vector_society.functions import simulation, simulation_toy_model
import networkx as nx
import numpy as np
import os
import pickle
import argparse
#import mkl
#mkl.set_num_threads(1)

def get_erdosh(seed, number_of_nodes, p):
    path = 'erdos_p_{}_nodes_{}_seed_{}.gpkl'.format(p, number_of_nodes, seed)
    if os.path.exists(path):
        G = nx.read_gpickle(path)
    else:
        G = nx.erdos_renyi_graph(number_of_nodes, p, seed=seed)
        G.remove_edges_from(G.selfloop_edges())
        # G = sorted(nx.connected_component_subgraphs(G), key=len)[-1]
        path = 'erdos_p_{}_nodes_{}_seed_{}.gpkl'.format(p, number_of_nodes,
                                                         seed)
        G = nx.convert_node_labels_to_integers(G)
        nx.write_gpickle(G, path)
    path = 'adj_p_{}_nodes_{}_seed_{}.pkl'.format(p, number_of_nodes, seed)
    if os.path.exists(path):
        with open(path) as f:
            adj = pickle.load(f)
    else:
        adj = nx.adj_matrix(G)
        pickle.dump(adj, open(path, 'w'), protocol=2)
    adj = adj.astype(int)
    return G, adj


def breakdown_trust(cog_dis, priorC, expected_nodes, ald, seed, network_seed=1,
                    save_last=False, priorC0=8, memory=12, pos_seed=1, G=None):
    # network_seed = 1
    param = 4.0
    prior0 = (priorC0, memory - priorC0)
    if G is None:
        G, adj = get_erdosh(network_seed, expected_nodes,
                            param / expected_nodes)
    else:
        adj = nx.adj_matrix(G)
    number_of_nodes = G.number_of_nodes()
    if ald == -1:
        if expected_nodes == 100000:
            ald_list = [0]
            ald_list.extend(range(1, 5000, 100))
            ald_list.extend(range(5000, 90000, 1000))
        else:
            ald_list = range(number_of_nodes)
    elif ald == -2:
        if expected_nodes == 100000:
            ald_list = range(0, 50000, 100)
    else:
        ald_list = [ald]
    justice = 0.3
    payoff = np.array([[1, 0], [1.1, 0.1]])
    payoff += np.eye(2) * justice
    tmax = 2000
    if ald == -2:
        tmax = 3000
    prior = np.array((priorC, memory - priorC)).astype(int)
    paranoia = paranoia_eq_prior(prior, prior0)
    max_k = max(G.degree().values())
    del G
    beta_lut = build_beta_lookuptable(memory, paranoia, cog_dis, max_k)
    results = []
    end_sim = False
    times_saturated = 0
    for ald_num in ald_list:
        if not end_sim:
            if pos_seed != -1:
                np.random.seed(pos_seed)
            ald = np.random.choice(range(number_of_nodes), size=ald_num,
                                   replace=False)
            result, sgc, sa = simulation(seed, number_of_nodes, tmax, prior,
                                         paranoia, cog_dis, payoff, adj, ald,
                                         beta_lut)
            if sgc == number_of_nodes and sa == number_of_nodes:
                times_saturated += 1
            if times_saturated == 10:
                end_sim = True
        if save_last:
            if end_sim:
                result[-1]['AlD'] = ald_num
            result[-1]['gigant component size'] = sgc
            result[-1]['size of active nodes'] = sa
            result[-1]['network_seed'] = network_seed
            results.append(result[-1].copy())
        else:
            if end_sim:
                for res in result:
                    res['AlD'] = ald_num
            results.extend(result)
    return results


def toy_model(cog_dis, paranoia, priorC, ald, G, seed, memory=12):
    adj = nx.adj_matrix(G)
    number_of_nodes = G.number_of_nodes()
    justice = 0.3
    payoff = np.array([[1, 0], [1.1, 0.1]])
    payoff += np.eye(2) * justice
    tmax = 200
    if hasattr(priorC, '__iter__'):
        prior = [(C, memory - C) for C in priorC]
    else:
        prior = np.array((priorC, memory - priorC))
    max_k = max(G.degree().values())
    beta_lut = build_beta_lookuptable(memory, paranoia, cog_dis, max_k)
    results, actions, priors = simulation_toy_model(seed, number_of_nodes,
                                                    tmax, prior, paranoia,
                                                    cog_dis, payoff, adj, ald,
                                                    beta_lut)
    return results, actions, priors


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Computes the evolultion of\
                                     the system')
    parser.add_argument('cog_dis', type=str, help='Cognitive Dissonance')
    parser.add_argument('PriorC', type=str,
                        help='Initial prior (number of cooperation)')
    parser.add_argument('--expected_nodes', type=int, default=100000,
                        help='Number of expected nodes in the network')
    parser.add_argument('--ald', type=int, default=-1, help='Number of AlD \
                        agents. If this parameter is not given it will be set \
                        to -1, meaning that a list of ald will be used.')
    parser.add_argument('--seed', type=int, default=1, help='random seed')
    parser.add_argument('--nseed', type=int, default=1, help='random seed for \
                        network')
    parser.add_argument('--pos_seed', type=int, default=1, help='random seed \
                        for ald agents positions in network. if this value \
                        is set to -1 the seed will not be set.')
    parser.add_argument('--prior_c_0', type=int, default=8, help='Prior to \
                        compare for computing paranoia.')
    parser.add_argument('--memory', type=int, default=12, help='memory')
    parser.add_argument('--save_last', action='store_true',
                        help='If just the final state should be saved.')
    args = parser.parse_args()
    results = breakdown_trust(float(args.cog_dis), float(args.PriorC),
                              args.expected_nodes, args.ald, args.seed,
                              args.nseed, args.save_last, args.prior_c_0,
                              args.memory, args.pos_seed)
    filename = 'parallel_out/seed{}_nseed{}_nodes{}_ald{}_cog_dis{}_prior{}.pkl'
    filename = filename.format(args.seed, args.nseed, args.expected_nodes,
                               args.ald, args.cog_dis, args.PriorC)
    with open(filename, 'wb') as f:
        pickle.dump(results, f, protocol=2)
