# README

Steps:
* install Anaconda:
 - scp neuro-andres.local:Downloads/Anaconda2-4.4.0-Linux-x86_64.sh ./
 - bash Anaconda2-4.4.0-Linux-x86_64.sh
* add anaconda to the path even in ssh adding this line in the begning of .bashrc
 - export PATH="/home/andres/anaconda2/bin:$PATH"
 * logout login to activate conda
* create dirs:
 - mkdir 10-economy; cd 10-economy
* install git: conda install git
* clone repo: git clone https://ababino@bitbucket.org/ababino/the_breakdown_of_trust.git
* cd the_breakdown_of_trust
* add neuro-andres as remote: git remote add neuro-andres ssh://andres@neuro-andres.local/home/andres/10-economy/the_breakdown_of_trust
* mkdir parallel_out
* install htop: conda install -c conda-forge htop=2.0.2
* create conda enviroment: conda env create -f environment.yml
* activate conda enviroment: source activate tbt
* run: python percolation.py 0 8 --expected_nodes 100000 --ald 1
* if you want to use other pcs run: ./run_parallel.sh percolation2d_last_maxr new
