from scipy.special import btdtri
import timeit
from society.functions import build_beta_lookuptable

if __name__ == '__main__':
    setup = """import numpy as np;\
               from scipy.special import  btdtr, btdtri;\
               from society.functions import build_beta_lookuptable;\
               m = 2000;\
               a = np.random.randint(0, m, (4000,));\
               b = m - a;\
               p = a / (a + b);"""
    count = 10

    t = timeit.Timer("btdtr(a, b, p)", setup=setup)
    print("btdtr:", t.timeit(count)/count, "sec")

    setup += """proba = btdtr(a, b, p) + 0.33;"""
    t = timeit.Timer("btdtri(a, b, proba)", setup=setup)
    print("btdtri:", t.timeit(count)/count, "sec")

    setup += """lup = build_beta_lookuptable(m, 0.33)"""
    t = timeit.Timer("np.array([lup['ppf'][x][y] for x, y in zip(b, a)])", setup=setup)
    print("btdtri:", t.timeit(count)/count, "sec")
