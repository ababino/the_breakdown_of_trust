from __future__ import division
from vector_society.functions import paranoia_eq_prior, build_beta_lookuptable
from vector_society.functions import simulation, simulation_toy_model
import networkx as nx
import numpy as np
import os
import sys
import pickle
import argparse
from IPython import embed


def get_erdosh(seed, number_of_nodes, p):
    path = 'erdos_p_{}_nodes_{}_seed_{}.gpkl'.format(p, number_of_nodes, seed)
    if os.path.exists(path):
        G = nx.read_gpickle(path)
    else:
        G = nx.erdos_renyi_graph(number_of_nodes, p, seed=seed)
        G.remove_edges_from(G.selfloop_edges())
        G = sorted(nx.connected_component_subgraphs(G), key=len)[-1]
        path = 'erdos_p_{}_nodes_{}_seed_{}.gpkl'.format(p, number_of_nodes, seed)
        G = nx.convert_node_labels_to_integers(G)
        nx.write_gpickle(G, path)
    path = 'adj_p_{}_nodes_{}_seed_{}.pkl'.format(p, number_of_nodes, seed)
    if os.path.exists(path):
        with open(path) as f:
            adj = pickle.load(f)
    else:
        adj = nx.adj_matrix(G)
        pickle.dump(adj, open(path, 'w'), protocol=2)
    return G, adj


def avoiding(cog_dis, paranoia, priorC, expected_nodes, seed, save_last=False,
             memory=12, G=None):
    network_seed = 1
    param = 4.0
    if G is None:
        G, adj = get_erdosh(network_seed, expected_nodes, param / expected_nodes)
    else:
        adj = nx.adj_matrix(G)
    number_of_nodes = G.number_of_nodes()
    justice = 0.3
    payoff = np.array([[1, 0], [1.1, 0.1]])
    payoff += np.eye(2) * justice
    tmax = 2000
    prior = np.array((priorC, memory - priorC))
    paranoia = paranoia_eq_prior((paranoia, memory - paranoia), (8, 4))
    max_k = max(G.degree().values())
    del G
    beta_lut = build_beta_lookuptable(memory, paranoia, cog_dis, max_k)
    results = []

    result = simulation(seed, number_of_nodes, tmax, prior, paranoia, cog_dis,
                        payoff, adj, [], beta_lut)
    if save_last:
        results.append(result[-1])
    else:
        results.extend(result)
    return results


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Computes the evolultion of\
                                     the system')
    parser.add_argument('cog_dis', type=str, help='Cognitive Dissonance')
    parser.add_argument('paranoia', type=int, default=8, help='Prior to \
                        compare for computing paranoia.')
    parser.add_argument('PriorC', type=int,
                        help='Initial prior (number of cooperation)')
    parser.add_argument('--expected_nodes', type=int, default=100000,
                        help='Number of expected nodes in the network')
    parser.add_argument('--seed', type=int, default=1, help='random seed')
    parser.add_argument('--memory', type=int, default=12, help='memory')
    parser.add_argument('--save_last', action='store_true',
                        help='If just the final state should be saved.')
    args = parser.parse_args()
    results = avoiding(float(args.cog_dis), args.paranoia, args.PriorC,
                       args.expected_nodes, args.seed, save_last=args.save_last,
                       memory=args.memory)
    filename = 'parallel_out/seed{}_nodes{}_cog_dis{}_paranoia{}.pkl'
    filename = filename.format(args.seed, args.expected_nodes, args.cog_dis,
                               args.paranoia)
    with open(filename, 'wb') as f:
        pickle.dump(results, f, protocol=2)
