from __future__ import division
from __future__ import unicode_literals

import numpy as np
from scipy import stats
from .. import agents
from .. import worlds
import pandas as pd

def test_avoid():
    dfs = []
    seed = 68120
    tf = 50
    payoff = pd.DataFrame({'C': {'C': 1, 'D': 1.1}, 'D': {'C': 0., 'D': 0.1}})

    prior = (11, 6)
    beta = stats.beta(prior[1], prior[0])
    th = beta.mean()
    paranoia = 0
    kwargs = {'paranoia': paranoia, 'prior_params': prior, 'justice': 0.3}
    w1 = worlds.LocalObsGridW(3,3, payoff.get_values())
    w1.add_agents(agents.ParanoidComplex, 9, **kwargs)
    np.random.seed(seed)
    w1.evolve(tf)
    df = w1.get_log()
    colname = 'prior : {}, paranoia: {}'.format(prior, paranoia)
    df[colname] = df['action'].map({'C': 0, 'D': 1})
    df_mean = pd.pivot_table(df, index=['time'], values=[colname]);
    dfs.append(df_mean)

    prior = (13, 4)
    beta = stats.beta(prior[1], prior[0])
    paranoia = max(0, beta.cdf(th) - beta.cdf(beta.mean()))
    kwargs = {'paranoia': paranoia, 'prior_params': prior, 'justice': 0.3}
    w2 = worlds.LocalObsGridW(3,3, payoff.get_values())
    w2.add_agents(agents.ParanoidComplex, 9, **kwargs)
    np.random.seed(seed)
    w2.evolve(tf)
    df = w2.get_log()
    colname = 'prior : {}, paranoia: {:.2f}'.format(prior, paranoia)
    df[colname] = df['action'].map({'C': 0, 'D': 1})
    df_mean = pd.pivot_table(df, index=['time'], values=[colname]);

    dfs.append(df_mean)
    df = pd.concat(dfs, axis=1)
    df = df.rename(columns={'time':'Time'})

    df2 = pd.read_pickle('avoid_trust_seed_68120.pkl')
    assert df.equals(df2)
