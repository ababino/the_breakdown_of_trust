from __future__ import division
from collections import defaultdict
from scipy import stats, special
import logging
import numpy as np
from matplotlib import pyplot as plt
#from cython_code.manipulations import manipulated_prior
from IPython import embed
logger = logging.getLogger('society.agents')


def manipulated_mean(a, b, paranoia):
    p = a / (a + b)
    proba = special.btdtr(a, b, p) + paranoia
    p = special.btdtri(a, b, proba)
    return np.array([1. - p, p])


def manipulated_prior(prior0, prior1, own_prior1, cog_dis):
    events = prior0 + prior1
    prior1 += cog_dis * own_prior1
    new_events = prior0 + prior1
    prior0 = prior0 * (events / new_events)
    prior1 = prior1 * (events / new_events)
    return prior0, prior1


def expected_reward(a, b, paranoia, m00, m01, m10, m11):
    p = a / (a + b)
    proba = special.btdtr(a, b, p) + paranoia
    p = special.btdtri(a, b, proba)
    ap = np.array([1. - p, p])
    M = np.array([[m00, m01], [m10, m11]])
    er = np.dot(M, ap)
    is_cooperative = er[0] > er[1] or abs(er[0] - er[1]) < 1e-15
    bin_proba = er[1]/(er[0] + er[1])
    return er, is_cooperative, bin_proba


class BaseAgent(object):
    """
    The BaseAgent is the Base Class for all agents.

    obsp is the observe probability.
    """

    time = 0

    def __init__(self, name, payoff, dispatcher, lookuptable={}, justice=0,
                 prior_params=(1, 1), paranoia=0, obsp=1.0, cog_dis=0):
        if prior_params[0] == 0 or prior_params[1] == 0:
            logger.warning('The beta distribution is problematic with this \
                           prior')
        self.name = name
        self.index = {'C': 0, 'D': 1}
        self.prior_params = np.array(prior_params)
        self.prior = defaultdict(self.default_prior)
        self.prior[self.name] = np.array((0, 0))
        self.ap = np.array((0, 0))
        self.payoff = payoff + np.eye(2) * justice
        self.dispatcher = dispatcher
        self.lookuptable = lookuptable
        self.paranoia = paranoia
        self.cog_dis = cog_dis
        self.obsp = obsp
        self.color = 'b'
        self.events = {}

    def __repr__(self):
        return 'agent' + str(self.name)

    def __hash__(self):
        return self.name

    def default_prior(self):
        return self.prior_params.copy()

    def reset(self):
        for key in self.prior:
            self.prior[key] = self.default_prior()

    def observe(self, event):
        if np.random.rand() < self.obsp:
            agent_src = event[0]
            action = event[2]
            self.prior[agent_src][action] += 1

    def forget(self, event):
        agent_src = event[0]
        action = event[2]
        self.prior[agent_src][action] -= 1

    def aggregated_belief(self):
        a = sum([x[0] for x in self.beliefs.values()])
        b = sum([x[1] for x in self.beliefs.values()])
        return a, b

    def action_probability(self, prior):
        p = prior[0] / (prior[0] + prior[1])
        return np.array([p, 1 - p])

    def expected_reward(self, ap):
        return np.dot(self.payoff, ap)

    def get_prior(self, other_agent):
        if other_agent.__class__ == Prestigious:
            return self.prior_params
        return self.prior[other_agent.name]

    def plot_prior(self, other_agent, ax=None, **pltkwargs):
        prior = self.get_prior(other_agent)
        beta = stats.beta(prior[1], prior[0])
        th = beta.mean()
        x = np.linspace(0, 1, 100)
        y = beta.pdf(x)
        if ax is None:
            fig, ax = plt.subplots(1, 1)
        ax.plot(x, y, label='Beta({},{})'.format(prior[1], prior[0]), **pltkwargs)
        ax.plot([th, th], [0, beta.pdf(th)], label='Mean')
        a = self.payoff[0, 0]
        b = self.payoff[1, 0]
        c = self.payoff[1, 1]
        p_d_star = 1 - c/(a-b+c)
        ax.axvline(p_d_star, label='Corruption Threshold')
        xticks = ax.get_xticks()
        xticks = np.concatenate((xticks, [th, p_d_star]))
        xtickslabels = ['{:.1f}'.format(x) for x in xticks]
        xtickslabels[-2] = '$m$'
        xtickslabels[-1] = '$p_d^*$'
        ax.set_xticks(xticks)
        ax.set_xticklabels(xtickslabels)
        ax.set_xlabel('Probablity of Defect')
        return ax

    def choose_action(self):
        pass

    def act(self, other_agent):
        action = self.choose_action(other_agent)
        event = (self.name, other_agent.name, action)
        event_type = (self.name, other_agent.name)
        self.dispatcher.add_event(event_type, event)


class HomoEconomicus(BaseAgent):

    """
    The HomoEconomicus always choose the action that maximize its reward
    expectation
    """

    def choose_action(self, other_agent):
        ap = self.action_probability(self.get_prior(other_agent))
        er = self.expected_reward(ap)
        if abs(er[0] - er[1]) < 1e-15:
            action = np.random.randint(2)
        else:
            action = er.argmax()
        return action


class Matching(BaseAgent):

    """
    The MatchingAgent use a probability matching stategy.
    """

    def choose_action(self, other_agent):
        ap = self.action_probability(self.get_prior(other_agent))
        er = self.expected_reward(ap)
        action = np.random.binomial(1, er[1]/(er[0] + er[1]))
        return action


class Complex(BaseAgent):

    """
    The ComplexAgent Behaves like the EconomicusAgent if the
    expected reward of cooparating is grater than the expected reward of
    defecting. If not it uses the matching strategy.
    """

    def choose_action(self, other_agent):
        prior = self.get_prior(other_agent)
        er, is_cooperative, bin_proba = expected_reward(prior[1], prior[0], self.paranoia, self.payoff[0, 0], self.payoff[0, 1], self.payoff[1, 0], self.payoff[1, 1])
        if is_cooperative:
            action = 0
        else:
            action = np.random.binomial(1, bin_proba)
        return action


class Paranoid(BaseAgent):

    """
    The beliefs of the ParanoidAgent changes towards thinking that the
    other agents act deshonestly.
    """

    def action_probability(self, other_agent):
        prior = self.get_prior(other_agent)
        ap = manipulated_mean(prior[1], prior[0], self.paranoia)
        return ap

    def plot_prior(self, other_agent, ax=None, **kwargs):
        ax = super(Paranoid, self).plot_prior(other_agent, ax,
                                                       **kwargs)
        if self.paranoia != 0:
            prior = self.get_prior(other_agent)
            beta = stats.beta(prior[1], prior[0])
            amount_of_probablity = min(beta.cdf(beta.mean()) + self.paranoia, 1)
            p = beta.ppf(amount_of_probablity)
            ax.plot([p, p], [0, beta.pdf(p)], label='Manipulated Mean')
            x = np.linspace(beta.mean(), p, 100)
            ax.fill_between(x, beta.pdf(x), 0, alpha=0.5)
            ax.set_xticks(list(ax.get_xticks()) + [p])
            xticklabels = list(ax.get_xticklabels())
            xticklabels[-1] = r'$\overline{m}$'
            ax.set_xticklabels(xticklabels)
        return ax


class ConvenientlyUpset(BaseAgent):

    """
    The ConvenientlyUpsetAgent adds its own prior to others.
    """

    def get_prior(self, other_agent):
        if other_agent.__class__ == Prestigious:
            return self.prior_params
        prior = self.prior[other_agent.name]
        prior = manipulated_prior(prior[0], prior[1], self.prior[self.name][1], self.cog_dis)
        return prior


class AllD(BaseAgent):

    def __init__(self, *args, **kwargs):
        super(AllD, self).__init__(*args, **kwargs)
        self.color = 'r'

    def choose_action(self, other_agent):
        return 1


class AllC(BaseAgent):

    def __init__(self, *args, **kwargs):
        super(AllC, self).__init__(*args, **kwargs)
        self.color = 'y'

    def choose_action(self, other_agent):
        return 0


class ParanoidEconomicus(HomoEconomicus, Paranoid):
    pass


class ParanoidMatching(Matching, Paranoid):
    pass


class ParanoidComplex(Complex, Paranoid):
    pass


class ConvenientlyUpsetParanoidComplex(ConvenientlyUpset, Complex, Paranoid):
    pass


class ConvenientlyUpsetComplex(ConvenientlyUpset, Complex):
    pass


class Prestigious(ConvenientlyUpsetParanoidComplex):
    pass
