from scipy import special

def manipulated_mean(double a, double b, double paranoia):
    cdef double p, proba
    p = a / (a + b)
    proba = special.btdtr(a, b, p) + paranoia
    return special.btdtri(a, b, proba)

def manipulated_prior(double prior0, double prior1, double own_prior1, double cog_dis):
    cdef double events, new_events
    events = prior0 + prior1
    prior1 += cog_dis * own_prior1
    new_events = prior0 + prior1
    prior0 = prior0 * (events / new_events)
    prior1 = prior1 * (events / new_events)
    return prior0, prior1
