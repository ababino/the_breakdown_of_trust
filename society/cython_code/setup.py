from distutils.core import setup
from Cython.Build import cythonize

setup(
  name = 'Cython funtions',
  ext_modules = cythonize("manipulations.pyx"),
)
