
class AsyncEventDispatcher(object):

    """
    Asynchronus event dispather.
    """

    def __init__(self):
        self.subscribers = {}
        self.events = {}

    def subscribe(self, method, event_type):
        try:
            self.subscribers[event_type].append(method)
        except KeyError:
            self.subscribers[event_type] = [method]

    def unsubscribe(self, method, event_type):
        try:
            index = self.subscribers[event_type].index(method)
            self.subscribers[event_type].pop(index)
        except KeyError:
            print(str(method) + 'not subscribed')

    def add_event(self, event_type, event):
        try:
            self.events[event_type].append(event)
        except KeyError:
            self.events[event_type] = [event]

    def del_event_type(self, event_type):
        try:
            self.events.pop(event_type, None)
        except KeyError:
            print(str(event_type) + ' not in events')

    def notify(self):
        for event_type, method_list in self.subscribers.items():
            for method in method_list:
                if event_type in self.events:
                    for event in self.events[event_type]:
                        method(event)
        self.events = {}

    def reset(self):
        self.__init__()
