"""
Useful functions.
"""
from __future__ import division
from agents import AllD
from scipy import stats
import numpy as np
from matplotlib import pyplot as plt
from collections import defaultdict
import networkx as nx
import random
from scipy.special import btdtr, btdtri


def corruped_fraction(world):
        power = sum([is_corrupted(edge) for edge in world.network.edges()])
        power = power / world.network.number_of_edges()
        return power


def beta_mode(a, b):
    """
    The mode of the beta distribution.
    """
    if a > 1 and b > 1:
        mode = float((a - 1)) / (a + b - 2)
    else:
        if a > b:
            mode = 1
        elif b > a:
            mode = 0
        elif a == b:
            mode = 0.5
    return mode


def is_corrupted(edge):
    if edge[0].__class__ == AllD or edge[1].__class__ == AllD:
        out = True
    else:
        ap = edge[0].action_probability(edge[1])
        er = edge[0].expected_reward(ap)
        test1 =  er[0] > er[1] or abs(er[0] - er[1]) < 1e-15
        ap = edge[1].action_probability(edge[0])
        er = edge[1].expected_reward(ap)
        test2 =  er[0] > er[1] or abs(er[0] - er[1]) < 1e-15
        if test1 and test2:
            out = False
        else:
            out = True
    return out


def p_stars(payoff, j):
    a = payoff.loc['C', 'C'] + j
    b = payoff.loc['D', 'C']
    c = payoff.loc['D', 'D'] + j
    p_c_star = c/(a-b+c)
    p_d_star = 1 - p_c_star
    return p_c_star, p_d_star


def paranoia_eq_prior(prior1, prior2):
    beta = stats.beta(prior1[1], prior1[0])
    mean2 = prior2[1] / sum(prior2)
    paranoia_th = beta.cdf(mean2) - beta.cdf(beta.mean())
    return paranoia_th


def rec_ddict():

    """
    Recursive default dictinary of defaut dictionaries
    """

    return defaultdict(rec_ddict)


def build_beta_lookuptable(n, paranoia):
    d = rec_ddict()
    a = np.arange(1, n)
    for b in range(1, n):
        bv = b * np.ones(n)
        mean = a / (a + b)
        amount_of_probability = btdtr(a, b, mean)
        for ai, bi, meani in zip(a, bv, mean):
            d['cdf'][bi][ai][meani] = amount_of_probability
        amount_of_probability += paranoia
        out = btdtri(a, b, amount_of_probability)
        for ai, bi, amount_of_probabilityi in zip(a, bv, amount_of_probability):
            d['ppf'][bi][ai][amount_of_probabilityi] = out
    return d


def plot_network(world, ax=None, pos=None, kwnodes={}, kwedges={}):
    ecolormap = {True: 'r', False: 'b'}
    G = world.network
    ecolors = [ecolormap[is_corrupted(edge)] for edge in G.edges()]
    ncolors = [node.color for node in G.nodes()]
    if pos is None:
        pos = {n[0]: n[1]['pos'] for n in G.nodes(data=True)}
    if ax is None:
        fig, ax = plt.subplots(1, 1)
    nx.draw_networkx_nodes(G, pos=pos, node_color=ncolors, ax=ax, **kwnodes)
    nx.draw_networkx_edges(G, pos=pos, edge_color=ecolors, ax=ax, **kwedges)
    return ax


def rand_exponential(x, scale=2):
    return np.random.exponential(scale=scale, size=x)


def build_network(dist, network_seed, param, expected_nodes=100):
    if dist == 'watts_strogats':
        G = nx.watts_strogatz_graph(expected_nodes, 4, param,
                                    seed=network_seed)
        return G
    random.seed(network_seed)
    number_of_nodes = 0
    while number_of_nodes < 0.9*float(expected_nodes):
        number_of_nodes = expected_nodes + 10
        if dist == 'exponential':
            sfun = rand_exponential
            kwds = {'scale': param}
            deglist = create_degree_sequence(number_of_nodes, sfunction=sfun,
                                             max_tries=500000, **kwds)
            G = nx.configuration_model(deglist, create_using=nx.Graph(),
                                       seed=None)

        elif dist == 'powerlaw':
            sfun = nx.utils.random_sequence.powerlaw_sequence
            kwds = {'exponent': param}
            deglist = create_degree_sequence(number_of_nodes, sfunction=sfun,
                                             max_tries=500000, **kwds)
            G = nx.configuration_model(deglist, create_using=nx.Graph(),
                                       seed=None)
        elif dist == 'random':
            G = nx.erdos_renyi_graph(expected_nodes, param / expected_nodes)
        else:
            msg = 'dist should be exponential, powerlaw or random'
            print(dist, msg)
        G.remove_edges_from(G.selfloop_edges())
        G = sorted(nx.connected_component_subgraphs(G), key=len)[-1]
        number_of_nodes = len(G)
    return nx.convert_node_labels_to_integers(G)
