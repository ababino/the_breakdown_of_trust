import random
import logging
import pandas as pd
import itertools as itt
import networkx as nx
import pickle
from functions import build_beta_lookuptable
from eventdispatcher import AsyncEventDispatcher
from agents import BaseAgent, expected_reward
logger = logging.getLogger('society.worlds')


class BaseWorld(object):

    """
    The BaseWorld is the base class for all worlds
    """
    def __init__(self, payoff, tolog=True):
        self.time = 0
        self.nsim = 0
        self.tolog = tolog
        self.log = []
        self.payoff = payoff
        self.agents = []
        self.edges = []
        self.lookuptables = {}
        self.dispatcher = AsyncEventDispatcher()

    def evolve(self, n_steps):
        if self.time == 0 and self.nsim == 0:
            BaseAgent.time = sum(self.agents[0].prior_params)
            self.setup_network()
            self.step()
            n_steps -= 1
        for i in range(n_steps):
            if self.tolog:
                if self.time == self.log[-1]['time'] + 1:
                    self.step()
                elif self.time < self.log[-1]['time'] + 1:
                    self.recorded_step()
            else:
                self.step()

    def aggregated_belief(self):
        a = sum([x.aggregated_belief()[0] for x in self.agents])
        b = sum([x.aggregated_belief()[1] for x in self.agents])
        return a, b

    def get_log(self):
        return pd.DataFrame(self.log)

    def add_agents(self, agent_class, population, **kwargs):
        lookuptable = {}
        for i in range(len(self.agents), len(self.agents) + population):
            agent = agent_class(i, self.payoff, self.dispatcher, lookuptable,
                                **kwargs)
            self.agents.append(agent)

    def interact(self, agent1, agent2):
        agent1.act(agent2)
        agent2.act(agent1)

    def step(self):
        for agent1, agent2 in self.edges:
            self.interact(agent1, agent2)
        self.dispatcher.notify()
        self.time += 1
        BaseAgent.time += 1

    def undo_step(self):
        self.time -= 1
        df = self.get_log()
        df = df[df['time'] == self.time]
        df['action'] = df['action'].map({'C': 0, 'D': 1})
        for ind in df.index:
            action = df.loc[ind, 'action']
            src = df.loc[ind, 'src']
            dst = df.loc[ind, 'dst']
            event = (src, dst, action)
            event_type = (-src, -dst)
            self.dispatcher.add_event(event_type, event)
        self.dispatcher.notify()

    def recorded_step(self):
        df = self.get_log()
        df = df[df['time'] == self.time]
        df['action'] = df['action'].map({'C': 0, 'D': 1})
        for ind in df.index:
            action = df.loc[ind, 'action']
            src = df.loc[ind, 'src']
            dst = df.loc[ind, 'dst']
            event = (src, dst, action)
            event_type = (src, dst)
            self.dispatcher.add_event(event_type, event)
        self.unsubscribe_log()
        self.dispatcher.notify()
        self.subscribe_log()
        self.time += 1
        BaseAgent.time += 1

    def setup_network(self):
        self.setup_interaction_network()
        self.setup_observation_network()
        if self.tolog:
            self.subscribe_log()

    def setup_interaction_network(self):
        """The edges list defines the interactions"""
        pass

    def setup_observation_network(self):

        """
        The subscriptions to the edges (agent1, agent2) defines the
        observations. The default behavior is that agents only see their own
        interactions.
        """

        for edge in self.edges:
            for agent in edge:
                event_type = (edge[0].name, edge[1].name)
                self.dispatcher.subscribe(agent.observe, event_type)
                self.dispatcher.subscribe(agent.observe, event_type[::-1])
                event_type = (-event_type[0], -event_type[1])
                self.dispatcher.subscribe(agent.forget, event_type)
                self.dispatcher.subscribe(agent.forget, event_type[::-1])

    def reset_dispatcher(self):

        """
        The subscriptions to the edges (agent1, agent2) defines the
        observations. The default behavior is that agents only see their own
        interactions.
        """

        self.dispatcher.reset()

    def subscribe_log(self):
        for edge in self.edges:
            edge_ind = (edge[0].name, edge[1].name)
            self.dispatcher.subscribe(self.write_in_log, edge_ind)
            self.dispatcher.subscribe(self.write_in_log, edge_ind[::-1])

    def unsubscribe_log(self):
        for edge in self.edges:
            edge_ind = (edge[0].name, edge[1].name)
            self.dispatcher.unsubscribe(self.write_in_log, edge_ind)
            self.dispatcher.unsubscribe(self.write_in_log, edge_ind[::-1])

    def write_in_log(self, event):
        src = event[0]
        dst = event[1]
        action = event[2]
        row = {'src': src,
               'dst': dst,
               'action': ['C', 'D'][action],
               'time': self.time,
               'nsim': self.nsim}
        self.log.append(row)

    def reset(self):
        for agent in self.agents:
            agent.reset()
        self.time = 0
        self.nsim += 1
        BaseAgent.time = sum(self.agents[0].prior_params)

    def save_lookuptable(self, filename):
        pickle.dump(self.lookuptable, open(filename, 'w'), protocol=2)


class DenseWorld(BaseWorld):

    """
    In the DenseWorld every agent interacts with each other agent every time
    step.
    """

    def setup_interaction_network(self):
        self.edges = list(itt.combinations(self.agents, 2))


class RandomWorld(BaseWorld):

    """
    In the RandomWorld every agent interacts with other agent randomly,
    independently of its location.
    """

    def step(self):
        logger.info("time %s" % self.time)
        for agent1 in self.agents:
            agent2 = random.choice(self.agents)
            logger.info("%s interacts with %s" % (agent1, agent2))
            self.interact(agent1, agent2)


class GlassWorld(BaseWorld):

    """
    Every agent sees the actions of all other agents.
    """

    def setup_observation_network(self):
        edges = list(itt.combinations(self.agents, 2))
        for edge, agent in itt.product(edges, self.agents):
            self.dispatcher.subscribe(agent.observe, edge)
            self.dispatcher.subscribe(agent.observe, edge[::-1])


class RandomGlassWorld(RandomWorld, GlassWorld):

    """
    In the RandomGlassWorld every agent interacts with other agent randomly,
    independently of its location. Being glass means that every agent sees the
    actions of other agents.
    """

    pass


class DenseGlassWorld(RandomWorld, GlassWorld):

    """
    In the DenseGlassWorld every agent interacts with every other agent. Being
    glass means that every agent sees the actions of other agents.
    """

    pass


class GridWorld(BaseWorld):

    """
    In the GridWorld every agent is an edge if a grid network. The interaction
    is between linked agents. The observation...
    """

    def __init__(self, n, m, *args, **kwargs):
        super(GridWorld, self).__init__(*args, **kwargs)
        self.n = n
        self.m = m
        self.network = nx.grid_2d_graph(n, m)
        for node in self.network:
            self.network.node[node]['pos'] = node
        self.node2agent = {}

    def add_agents(self, agent_class, population, **kwargs):
        if len(self.agents) + population > self.network.number_of_nodes():
            print('more agentes than nodes')
        super(GridWorld, self).add_agents(agent_class, population, **kwargs)

    def setup_interaction_network(self):
        for node in self.network.nodes():
            self.node2agent[node] = self.agents[node[0]*self.m + node[1]]
        self.network = nx.relabel_nodes(self.network, self.node2agent)
        self.edges = self.network.edges()


class NetWorld(BaseWorld):

    """
    Arbitrary network interaction world.
    """

    def __init__(self, network, *args, **kwargs):
        super(NetWorld, self).__init__(*args, **kwargs)
        self.network = network
        self.node2agent = {}

    def add_agents(self, agent_class, population, **kwargs):
        if len(self.agents) + population > self.network.number_of_nodes():
            print('more agentes than nodes')
        super(NetWorld, self).add_agents(agent_class, population, **kwargs)

    def setup_interaction_network(self):
        self.node2agent = {}
        for node, agent in zip(self.network.nodes(), self.agents):
            self.node2agent[node] = agent
        self.network = nx.relabel_nodes(self.network, self.node2agent)
        self.edges = self.network.edges()


class LocalObsWorld(BaseWorld):

    """
    In the LocalObsWorld agents see the actions of their neighbors.
    """

    def setup_observation_network(self):
        G = nx.Graph()
        G.add_edges_from(self.edges)
        for edge in self.edges:
            edge_ind = (edge[0].name, edge[1].name)
            self.dispatcher.subscribe(edge[0].observe, edge_ind)
            self.dispatcher.subscribe(edge[1].observe, edge_ind[::-1])
            for neighbor in G.neighbors_iter(edge[0]):
                self.dispatcher.subscribe(neighbor.observe, edge_ind)
            for neighbor in G.neighbors_iter(edge[1]):
                self.dispatcher.subscribe(neighbor.observe, edge_ind[::-1])
            inv_edge = (-edge[0].name, -edge[1].name)
            self.dispatcher.subscribe(edge[0].forget, inv_edge)
            self.dispatcher.subscribe(edge[1].forget, inv_edge[::-1])
            for neighbor in G.neighbors_iter(edge[0]):
                self.dispatcher.subscribe(neighbor.forget, inv_edge)
            for neighbor in G.neighbors_iter(edge[1]):
                self.dispatcher.subscribe(neighbor.forget, inv_edge[::-1])


class LocalObsGridW(GridWorld, LocalObsWorld):
    pass


class LocalObsNetW(NetWorld, LocalObsWorld):
    pass
