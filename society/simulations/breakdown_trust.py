from __future__ import division
from __future__ import unicode_literals
import numpy as np
from scipy import stats
from society.agents import AllD, Prestigious
from society.agents import Complex
from society.agents import ParanoidComplex as CUC
from society.agents import ConvenientlyUpsetParanoidComplex as MCUC
from society.agents import ConvenientlyUpsetComplex as MC
from society.worlds import LocalObsNetW, NetWorld
from society.functions import corruped_fraction, paranoia_eq_prior, p_stars
from society.functions import build_network
import pandas as pd
import random
import sys
import pickle


def breakdown_trust(network_seed, param, number_of_nodes, alld, niters=1):
    dist = 'random'
    obsp = 1.0
    G = build_network(dist, network_seed, param,
                      expected_nodes=number_of_nodes)
    number_of_nodes = len(G)
    number_of_edges = G.number_of_edges()

    results = []
    tf = 100
    payoff = pd.DataFrame({'C': {'C': 1, 'D': 1.1}, 'D': {'C': 0., 'D': 0.1}})
    evolution_seeds = [random.randint(0, 4294967295) for _ in range(niters)]
    prior = (10, 2)
    justice = 0.3
    beta = stats.beta(prior[1], prior[0])
    p_c, p_d = p_stars(payoff, justice)
    paranoia_th = beta.cdf(p_d) - beta.cdf(beta.mean())
    kw1 = {'paranoia': paranoia_th, 'prior_params': prior, 'justice': justice,
           'obsp': obsp, 'cog_dis': 1.0}
    kw2 = {'paranoia': 0, 'prior_params': (8, 4), 'justice': justice,
           'obsp': obsp}
    agent_kw_list = [(MCUC, kw1)] #(Complex, kw2), (MC, kw2), (CUC, kw1),
    for Agent, kw in agent_kw_list:
        w = NetWorld(G, payoff.get_values(), tolog=False)
        agents = sum([[Agent] * (number_of_nodes - alld), [AllD] * alld], [])
        random.shuffle(agents)
        for agent in agents:
            w.add_agents(agent, 1, **kw)
        w.setup_network()
        result = {'Distribution': dist,
                  'param': param,
                  'Number of Nodes': number_of_nodes,
                  'Number of Edges': number_of_edges,
                  'network_seed': network_seed,
                  'Observation Probability': obsp,
                  'World': w.__class__.__name__,
                  'Initial Conditions': 'prior={}, paranoia={}'.format(kw['prior_params'], kw['paranoia']),
                  'Agent': Agent.__name__}
        for evolution_seed in evolution_seeds:
            np.random.seed(evolution_seed)
            for i in range(tf):
                power = corruped_fraction(w)
                result['Fraction of Corrupted Edges'] = power
                result['evolution_seed'] = evolution_seed
                result['Time'] = w.time
                results.append(result.copy())
                w.step()
            w.reset()

    filename = 'parallel_out/seed{}_{}_{}_obsp{}_node{}.pkl'
    filename = filename.format(network_seed, dist, param, obsp,
                               number_of_nodes)
    with open(filename, 'wb') as f:
        pickle.dump(results, f, protocol=2)


if __name__ == '__main__':
    breakdown_trust(int(sys.argv[1]), float(sys.argv[2]), int(sys.argv[3]),
                    int(sys.argv[4]))
