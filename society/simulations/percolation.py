from __future__ import division
import random
from itertools import product
import pandas as pd
from scipy import stats
from society.worlds import NetWorld as NetW
from society.worlds import LocalObsNetW as ObsW
from society.agents import AllD, Prestigious
from society.agents import Complex
from society.agents import ParanoidComplex as CUC
from society.agents import ConvenientlyUpsetParanoidComplex as MCUC
from society.agents import ConvenientlyUpsetComplex as MC

from society.functions import p_stars, is_corrupted, build_network
import networkx as nx
import numpy as np
import pickle
import sys


class InputError(Exception):
    """Exception raised for errors in the input.

    Attributes:
        expr -- input expression in which the error occurred
        msg  -- explanation of the error
    """

    def __init__(self, expr, msg):
        self.expr = expr
        self.msg = msg


def percolation(network_seed, dist, param, obsp=1.0, worlds=['Net'],
                alld_list=[10], tf=50, nodes=10000):

    """
    Percolation function. dist can be exponential or powerlaw.
    """
    results = []
    payoff = pd.DataFrame({'C': {'C': 1, 'D': 1.1}, 'D': {'C': 0., 'D': 0.1}})
    prior = (10, 2)
    justice = 0.3
    beta = stats.beta(prior[1], prior[0])
    p_c, p_d = p_stars(payoff, justice)
    paranoia_th = beta.cdf(p_d) - beta.cdf(beta.mean())
    kw1 = {'paranoia': paranoia_th, 'prior_params': prior, 'justice': justice,
           'obsp': obsp}
    kw2 = {'paranoia': 0, 'prior_params': (8, 4), 'justice': justice,
           'obsp': obsp}
    agent_kw_list = [(Complex, kw2), (MC, kw2), (CUC, kw1), (MCUC, kw1)]
    world_dic = {'Net': NetW, 'Loc': ObsW}
    world_list = [world_dic[w] for w in worlds]
    G = build_network(dist, network_seed, param, expected_nodes=nodes)
    number_of_nodes = len(G)
    number_of_edges = G.number_of_edges()

    result = {'Distribution': dist,
              'param': param,
              'Number of Nodes': number_of_nodes,
              'Number of Edges': number_of_edges,
              'network_seed': network_seed,
              'Observation Probability': obsp,
              'Final Time': tf}

    for _ in range(50):
        evolution_seed = random.randint(0, sys.maxint)
        result['evolution_seed'] = evolution_seed
        test = False
        for World, (Agent, kwargs) in product(world_list, agent_kw_list):
            result['World'] = World.__name__
            result['Agent'] = Agent.__name__
            w = World(G, payoff.get_values(), tolog=False)
            for alld_nodes, prest_nodes in product(alld_list, [0]):
                if test:
                    result['Number of AllD'] = alld_nodes
                    result['Prestigious Nodes'] = prest_nodes
                    result['Fraction of Corrupted Edges'] = power
                    results.append(result)
                    continue
                random.seed(evolution_seed)
                mcu_nodes = number_of_nodes-alld_nodes - prest_nodes
                node_types = [[Agent] * mcu_nodes,
                              [AllD] * alld_nodes, [Prestigious] * prest_nodes]
                node_types = sum(node_types, [])
                random.shuffle(node_types)
                for node_type in node_types:
                        w.add_agents(node_type, 1, **kwargs)
                w.setup_network()
                w.evolve(tf)
                power = sum([is_corrupted(edge) for edge in w.network.edges()])
                power = power / w.network.number_of_edges()
                w.reset_dispatcher()
                w.agents = []
                result['Number of AllD'] = alld_nodes
                result['Prestigious Nodes'] = prest_nodes
                result['Fraction of Corrupted Edges'] = power
                results.append(result.copy())
                test = results[-1]['Fraction of Corrupted Edges'] == 1

    filename = 'parallel_out/{}_{}_{}_{}.pkl'.format(network_seed, dist, param,
                                                     obsp)
    with open(filename, 'w') as f:
        pickle.dump(results, f, protocol=2)


if __name__ == '__main__':
    percolation(int(sys.argv[1]), sys.argv[2], float(sys.argv[3]),
                float(sys.argv[4]))
