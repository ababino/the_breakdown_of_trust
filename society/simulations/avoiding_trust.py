from __future__ import division
from __future__ import unicode_literals
import numpy as np
from society.agents import ParanoidComplex
from society.worlds import LocalObsNetW
from society.functions import corruped_fraction, paranoia_eq_prior
from society.functions import build_network
import pandas as pd
import random
import sys
import pickle


def avoiding_trust(network_seed, dist, param, obsp):
    G = build_network(dist, network_seed, param)
    number_of_nodes = len(G)
    number_of_edges = G.number_of_edges()

    results = []
    tf = 50
    payoff = pd.DataFrame({'C': {'C': 1, 'D': 1.1}, 'D': {'C': 0., 'D': 0.1}})
    priors = [(11, 6), (13, 4)]
    paranoias = [0, paranoia_eq_prior(priors[1], priors[0])]
    evolution_seeds = [random.randint(0, sys.maxint) for _ in range(50)]
    kwargs = {'justice': 0.3, 'obsp': obsp}
    for prior, paranoia in zip(priors, paranoias):
        kwargs['paranoia'] = paranoia
        kwargs['prior_params'] = prior
        w = LocalObsNetW(G, payoff.get_values(), tolog=False)
        w.add_agents(ParanoidComplex, 100, **kwargs)
        w.setup_network()
        result = {'Distribution': dist,
                  'param': param,
                  'Number of Nodes': number_of_nodes,
                  'Number of Edges': number_of_edges,
                  'network_seed': network_seed,
                  'Observation Probability': obsp,
                  'World': w.__class__.__name__,
                  'Initial Conditions': 'prior={}, paranoia={}'.format(prior, paranoia)}
        for evolution_seed in evolution_seeds:
            np.random.seed(evolution_seed)
            for i in range(tf):
                power = corruped_fraction(w)
                result['Fraction of Corrupted Edges'] = power
                result['evolution_seed'] = evolution_seed
                result['Time'] = w.time
                results.append(result.copy())
                w.step()
            w.reset()

    filename = 'parallel_out/{}_{}_{}_{}.pkl'.format(network_seed, dist, param,
                                                     obsp)
    with open(filename, 'w') as f:
        pickle.dump(results, f, protocol=2)


if __name__ == '__main__':
    avoiding_trust(int(sys.argv[1]), sys.argv[2], float(sys.argv[3]),
                float(sys.argv[4]))
