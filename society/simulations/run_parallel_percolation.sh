
NODES=$(cat nodes_night.txt)

SSHLOGINS=$(echo --sshlogin $NODES)
SEEDS=$(for i in `seq 101 150`; do echo $i; done)
PARAMS=$(for i in `LANG=en_US seq 0.0 0.2 1.0`; do echo $i; done)
WORLDS='watts_strogats'
OBS=$(for i in `LANG=en_US seq 0.0 0.1 1.0`; do echo $i; done)

OUTNAME="new_ConvenientlyUpset_percolation.pkl"

echo "pulling updates in remotes"
SEP_NODES=${NODES//,/ }
for node in $SEP_NODES;
do
    node=${node/[0-9]\//};
    if [ "$node" != ":" ]; then
        echo $node
        ssh $node 'cd /home/andres/10-economy/the_breakdown_of_trust; git pull neuro-andres master'
        ssh $node 'cd /home/andres/10-economy/the_breakdown_of_trust;  if [ ! -d parallel_out/ ]; then mkdir parallel_out; fi'
        ssh $node 'cd /home/andres/10-economy/the_breakdown_of_trust; rm parallel_out/*.pkl'
    fi
done

echo "running percolation"
parallel --joblog job-pipeline.tsv --resume --tag  --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/{1}_{2}_{3}_{4}.pkl "cd ~/10-economy/the_breakdown_of_trust/; python percolation.py {1} {2} {3} {4}" ::: $SEEDS ::: $WORLDS ::: $PARAMS ::: $OBS


echo "running merge"
python -c "import pandas as pd; from glob import glob; from pickle import load; df = pd.concat([pd.DataFrame(load(open(f))) for f in glob('parallel_out/*.pkl')]); df.to_pickle('data/$OUTNAME')"
