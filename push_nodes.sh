
echo $1
if [ "$1" = "" ]; then
    branch="master";
else
    branch="$1";
fi
NODES=$(cat nodes_night.txt)
SEP_NODES=${NODES//,/ }
for node in $SEP_NODES;
do
    node=${node/[0-9]\//};
    if [ "$node" != ":" ]; then
        echo $node
        ssh $node 'cd /home/andres/10-economy/the_breakdown_of_trust; git checkout '$branch
        ssh $node 'cd /home/andres/10-economy/the_breakdown_of_trust; git pull neuro-andres '$branch
        ssh $node 'cd /home/andres/10-economy/the_breakdown_of_trust;  if [ ! -d parallel_out/ ]; then mkdir parallel_out; fi'
        ssh $node 'cd /home/andres/10-economy/the_breakdown_of_trust; rm parallel_out/*.pkl'
    fi
done
