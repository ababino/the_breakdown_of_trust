#!/usr/bin/env bash
#run_parallel.sh key [new]
branch=$(git rev-parse --abbrev-ref HEAD)
bash push_nodes.sh $branch
key="$1"
OUTNAME="$key.pkl"
if [ "$2" = "new" ]; then
    echo "You will erase your last unfinished run. Are you sure? (y/n)"
    read ans
    if [ "$ans" = "y" ]; then
        rm parallel_out/*.pkl;
        rm job-pipeline.tsv;
    fi
fi
case $key in
    percolation2d_last_maxr_cooppr_pr_vs_cd)
    SEED=`seq 1 10`;
    NSEED=`seq 1 5`;
    COG_DIS=$(for i in `LANG=en_US seq 0.0 0.25 0.75`; do echo $i; done)
    PRIOR=$(for i in `LANG=en_US seq 8 11`; do echo $i; done)
    if [ "$ans" = "y" ]; then
        rm job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd.tsv;
    fi
    echo "running percolation"
    parallel --joblog job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{1}_nseed{2}_nodes100000_ald-1_cog_dis{4}_prior{3}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py {4} {3} --seed {1} --nseed {2} --save_last" ::: $SEED ::: $NSEED ::: $PRIOR ::: $COG_DIS
    ;;

    percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold1_no_in_threshold1)
    SEED=`seq 1 10`;
    NSEED=`seq 1 5`;
    COG_DIS=$(for i in `LANG=en_US seq 0.0 0.25 0.75`; do echo $i; done)
    PRIOR=$(for i in `LANG=en_US seq 9 11`; do echo $i; done)
    if [ "$ans" = "y" ]; then
        rm job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold1.tsv;
    fi
    echo "running percolation"
    parallel --joblog job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold1.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{1}_nseed{2}_nodes100000_ald-1_cog_dis{4}_prior{3}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py {4} {3} --seed {1} --nseed {2} --save_last --prior_c_0 9" ::: $SEED ::: $NSEED ::: $PRIOR ::: $COG_DIS
    ;;

    percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold1_no_in_threshold2)
    SEED=`seq 1 10`;
    NSEED=`seq 1 5`;
    COG_DIS=$(for i in `LANG=en_US seq 0.0 0.25 1.0`; do echo $i; done)
    PRIOR=$(for i in `LANG=en_US seq 10 11`; do echo $i; done)
    if [ "$ans" = "y" ]; then
        rm job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold2.tsv;
    fi
    echo "running percolation"
    parallel --joblog job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold2.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{1}_nseed{2}_nodes100000_ald-1_cog_dis{4}_prior{3}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py {4} {3} --seed {1} --nseed {2} --save_last --prior_c_0 10" ::: $SEED ::: $NSEED ::: $PRIOR ::: $COG_DIS
    ;;

    percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold1_no_in_threshold22)
    SEED=`seq 1 10`;
    NSEED=`seq 1 5`;
    PRIOR=$(for i in `LANG=en_US seq 10 11`; do echo $i; done)
    if [ "$ans" = "y" ]; then
        rm job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold22.tsv;
    fi
    echo "running percolation"
    parallel --joblog job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold22.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{1}_nseed{2}_nodes100000_ald-1_cog_dis1.25_prior{3}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py 1.25 {3} --seed {1} --nseed {2} --save_last --prior_c_0 10" ::: $SEED ::: $NSEED ::: $PRIOR
    ;;

    percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold23)
    SEED=`seq 1 10`;
    NSEED=`seq 1 5`;
    COG_DIS=$(for i in `LANG=en_US seq 1.5 0.25 2.0`; do echo $i; done)
    PRIOR=$(for i in `LANG=en_US seq 10 11`; do echo $i; done)
    if [ "$ans" = "y" ]; then
        rm job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold23.tsv;
    fi
    echo "running percolation"
    parallel --joblog job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold23.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{1}_nseed{2}_nodes100000_ald-1_cog_dis{4}_prior{3}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py {4} {3} --seed {1} --nseed {2} --save_last --prior_c_0 10" ::: $SEED ::: $NSEED ::: $PRIOR ::: $COG_DIS
    ;;

    percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold24)
    SEED=`seq 1 10`;
    NSEED=`seq 1 5`;
    COG_DIS=$(for i in `LANG=en_US seq 2.25 0.25 3.0`; do echo $i; done)
    PRIOR=$(for i in `LANG=en_US seq 10 11`; do echo $i; done)
    if [ "$ans" = "y" ]; then
        rm job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold24.tsv;
    fi
    echo "running percolation"
    parallel --joblog job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_no_in_threshold24.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{1}_nseed{2}_nodes100000_ald-1_cog_dis{4}_prior{3}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py {4} {3} --seed {1} --nseed {2} --save_last --prior_c_0 10" ::: $SEED ::: $NSEED ::: $PRIOR ::: $COG_DIS
    ;;


    percolation2d_last_maxr_cooppr_pr_vs_cd_more_memory)
    SEED=`seq 1 10`;
    NSEED=`seq 1 5`;
    COG_DIS=$(for i in `LANG=en_US seq 0.0 0.25 0.75`; do echo $i; done)
    PRIOR=$(for i in `LANG=en_US seq 12 17`; do echo $i; done)
    if [ "$ans" = "y" ]; then
        rm job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_more_memory.tsv;
    fi
    echo "running percolation"
    parallel --joblog job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_more_memory.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{1}_nseed{4}_nodes100000_ald-1_cog_dis{3}_prior{2}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py {3} {2} --seed {1} --save_last --nseed {4} --prior_c_0 12 --memory 18" ::: $SEED ::: $PRIOR ::: $COG_DIS ::: $NSEED
    ;;

    percolation2d_last_maxr_cooppr_pr_vs_cd_more_memory2)
    SEED=`seq 1 10`;
    NSEED=`seq 1 5`;
    COG_DIS=$(for i in `LANG=en_US seq 0.0 0.25 0.75`; do echo $i; done)
    PRIOR=$(for i in `LANG=en_US seq 10 13`; do echo $i; done)
    if [ "$ans" = "y" ]; then
        rm job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_more_memory2.tsv;
    fi
    echo "running percolation"
    parallel --joblog job-pipeline-percolation2d_last_maxr_cooppr_pr_vs_cd_more_memory2.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{1}_nseed{4}_nodes100000_ald-1_cog_dis{3}_prior{2}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py {3} {2} --seed {1} --save_last --nseed {4} --prior_c_0 10 --memory 14" ::: $SEED ::: $PRIOR ::: $COG_DIS ::: $NSEED
    ;;

    percolation2d_last_maxr_cooppr_fad_vs_cd)
    SEED=`seq 1 10`;
    NSEED=`seq 1 5`;
    COG_DIS=$(for i in `LANG=en_US seq 0.0 0.05 2.00`; do echo $i; done)
    PRIOR="9"
    if [ "$ans" = "y" ]; then
        rm job-pipeline-percolation2d_last_maxr_cooppr_fad_vs_cd.tsv;
    fi
    echo "running percolation"
    parallel --joblog job-pipeline-percolation2d_last_maxr_cooppr_fad_vs_cd.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{1}_nseed{2}_nodes100000_ald-1_cog_dis{4}_prior{3}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py {4} {3} --seed {1} --nseed {2} --save_last" ::: $SEED ::: $NSEED ::: $PRIOR ::: $COG_DIS
    ;;

    percolation2d_last_maxr_cooppr_fad_vs_cd2)
    SEED=`seq 1 10`;
    NSEED=`seq 1 5`;
    COG_DIS=$(for i in `LANG=en_US seq 0.0 0.05 2.00`; do echo $i; done)
    PRIOR="9"
    if [ "$ans" = "y" ]; then
        rm job-pipeline-percolation2d_last_maxr_cooppr_fad_vs_cd2.tsv;
    fi
    echo "running percolation"
    parallel --joblog job-pipeline-percolation2d_last_maxr_cooppr_fad_vs_cd2.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{1}_nseed{2}_nodes100000_ald-2_cog_dis{4}_prior{3}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py {4} {3} --seed {1} --nseed {2} --save_last --ald=-2" ::: $SEED ::: $NSEED ::: $PRIOR ::: $COG_DIS
    ;;

    percolation2d_last_maxr_cooppr_onefad_vs_cd)
    SEED=`seq 1 10`;
    NSEED=`seq 1 5`;
    COG_DIS=$(for i in `LANG=en_US seq 0.0 0.05 4.00`; do echo $i; done)
    PRIOR=$(for i in `LANG=en_US seq 8 11`; do echo $i; done)
    if [ "$ans" = "y" ]; then
        rm job-pipeline-percolation2d_last_maxr_cooppr_onefad_vs_cd.tsv;
    fi
    echo "running percolation"
    parallel --joblog job-pipeline-percolation2d_last_maxr_cooppr_onefad_vs_cd.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{1}_nseed{2}_nodes100000_ald1_cog_dis{4}_prior{3}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py {4} {3} --seed {1} --nseed {2} --save_last --ald=1" ::: $SEED ::: $NSEED ::: $PRIOR ::: $COG_DIS
    ;;

    percolation2d_last2)
    SEED=`seq 1 100`;
    COG_DIS=$(for i in `LANG=en_US seq 0.0 0.25 0.75`; do echo $i; done)
    PRIOR=$(for i in `LANG=en_US seq 8 11`; do echo $i; done)
    echo "running percolation"
    parallel --joblog job-pipeline.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{3}_nodes100000_ald-1_cog_dis{1}_prior{2}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py {1} {2} --seed {3} --save_last" ::: $COG_DIS ::: $PRIOR ::: $SEED
    ;;

    bifurcation6)
        SEED=`seq 1 100`;
    COG_DIS=$(for i in `LANG=en_US seq 0.0 0.25 0.75`; do echo $i; done)
    PRIOR=$(for i in `LANG=en_US seq 7 10`; do echo $i; done)
    echo "running percolation"
    parallel --joblog job-pipeline.tsv --resume-failed --tag --delay 1 --slf nodes.txt --return ~/10-economy/the_breakdown_of_trust/parallel_out/seed{2}_nodes100000_ald0_cog_dis{3}_prior{1}.pkl "cd ~/10-economy/the_breakdown_of_trust/;source activate tbt; python percolation.py {3} {1} --seed {2} --ald 0 --prior_c_0 7" ::: $PRIOR ::: $SEED ::: $COG_DIS
    ;;
    *)
    echo 'argument must be percolation or avoiding'
esac

echo "running merge"
python -c "import pandas as pd; from glob import glob; from pickle import load; df = pd.concat([pd.DataFrame(load(open(f))) for f in glob('parallel_out/*.pkl')]); df.to_pickle('data/$OUTNAME')"
