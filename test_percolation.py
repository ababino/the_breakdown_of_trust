import unittest
import pickle
from percolation import breakdown_trust
from vector_society.functions import paranoia_eq_prior, build_beta_lookuptable
import numpy as np
from IPython import embed


class TestPercolation(unittest.TestCase):
    def test_breakdown_against_master(self):
        network_seed = 1
        param = 4
        expected_nodes = 1000
        alld = 1
        cog_dis = 0.5
        priorC = 8
        results0 = breakdown_trust(cog_dis, priorC, expected_nodes, alld, 1)
        filename = 'seed{}_param{}_nodes{}_alld{}_cog_dis{}_prior{}_master.pkl'
        filename = filename.format(network_seed, param, expected_nodes, alld,
                                   cog_dis, priorC)
        with open(filename) as f:
            results1 = pickle.load(f)
        self.assertEqual(results0, results1)

    def test_breakdown_against_delprobamat(self):
        network_seed = 1
        param = 4
        expected_nodes = 1000
        alld = 1
        cog_dis = 0.5
        priorC = 8
        results0 = breakdown_trust(cog_dis, priorC, expected_nodes, alld, 1)
        filename = 'seed{}_nodes{}_ald{}_cog_dis{}_prior{}_delprobamat.pkl'
        filename = filename.format(network_seed, expected_nodes, alld,
                                   cog_dis, priorC)
        with open(filename) as f:
            results1 = pickle.load(f)
        self.assertEqual(results0, results1)

    def test_blut_against_master(self):
        priorC0 = 8
        priorC = 9
        memory = 12
        cog_dis = 0.1
        prior0 = (priorC0, memory - priorC0)
        prior = np.array((priorC, memory - priorC)).astype(int)
        paranoia = paranoia_eq_prior(prior, prior0)
        max_k = 10
        beta_lut = build_beta_lookuptable(memory, paranoia, cog_dis, max_k)
        filename = 'priorC0{}_priorC{}_memory{}_maxk{}_cogdis{}_master.pkl'
        filename = filename.format(priorC0, priorC, memory, max_k, cog_dis)
        with open(filename) as f:
            beta_lut_master = pickle.load(f)
        np.testing.assert_array_equal(beta_lut, beta_lut_master)
        # self.assertEqual(beta_lut, beta_lut_master)


    def test_nans(self):
        network_seed = 1
        param = 4
        expected_nodes = 1000
        alld = 1
        cog_dis = 0.5
        priorC = 8
        results = breakdown_trust(cog_dis, priorC, expected_nodes, alld, 1)
        for result in results:
            for key in result:
                if type(result[key]) == float:
                    self.assertFalse(np.isnan(result[key]))


if __name__ == '__main__':
    unittest.main()
