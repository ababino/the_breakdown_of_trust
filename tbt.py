"""
The Breakdown of Trust
"""
from __future__ import division
from society.agents import HomoEconomicus, ParanoidAgent, FairAgent
from society.worlds import RandomWorld
from scipy.stats import beta
from numpy import arange, array
from matplotlib import pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np

payoff = pd.DataFrame({'C': {'C': 1, 'D': 1.1}, 'D': {'C': 0., 'D': 0.2}})

payoffM = payoff.get_values()
kwargs = {'paranoia': 0.1, 'prior_params': (5, 2), 'justice': 0.3}
brw = RandomWorld(10, ParanoidAgent, payoffM, **kwargs)
brw.evolve(100)
df = brw.get_log()
df

a1 = brw.agents[0]
a2 = brw.agents[1]
a1.prior[a2.name] = np.array([5, 2])
a1.expected_reward(a2)

df = pd.concat(df, names=['paranoia'])
df.reset_index(level=0, inplace=True)
act_per_agent = pd.pivot_table(df[['paranoia', 'src', 'action']],
                               columns=['action'],
                               index=['paranoia', 'src'], aggfunc=len)
act_per_agent.reset_index(level=0, inplace=True)
act_per_agent.eval('ratio = C/(C+D)')
g = sns.FacetGrid(act_per_agent, row="paranoia", margin_titles=True)
bins = np.linspace(0.5, 1, 100)
g.map(plt.hist, "ratio", bins=bins)
plt.show()


act_per_interaction = pd.pivot_table(brw.get_log(), columns=['action'],
                                     index=['src', 'dst'], aggfunc=len)

act_per_interaction.eval('ratio = C/(C+D)')

sns.distplot(act_per_agent['ratio'], 20)



sns.distplot(act_per_interaction['ratio'].dropna(), 20)
plt.show()


plt.plot(sorted(agg_act['cooperation']))
plt.show()
