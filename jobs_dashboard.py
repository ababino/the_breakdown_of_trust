"""Dashboard for parallel jobs."""
import time
import os.path
from bokeh.layouts import widgetbox, row, column, layout
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import DataTable, TableColumn, NumberFormatter, DateFormatter
from bokeh.plotting import curdoc, figure
import numpy as np
import pandas as pd


def load_df():
        df = pd.read_csv('job-pipeline.tsv', sep='\t')
        df["EndTime"] = df["Starttime"] + df["JobRuntime"]
        df["EndTime"] = pd.to_datetime(df['EndTime'], unit='s')
        df = df.sort_values('EndTime', ascending=True)
        df['Errors'] = (df['Exitval'] != 0).cumsum()
        df['Jobs Done'] = (df['Exitval'] == 0).cumsum()
        df = df.sort_values('EndTime', ascending=False)
        reg = '.+ (?P<Proyection>\d\.\d\d) (?P<Paranoia>\d\d?) --seed (?P<seed>\d\d?\d?).+'
        new_cols = df['Command'].str.extract(reg, expand=False)
        df[['Proyection', 'Paranoia', 'seed']] = new_cols

        counts = df.pivot_table(columns='Host', index='Exitval',
                                values='JobRuntime', aggfunc=len, margins=True)
        counts = counts.reset_index()
        counts.columns = counts.columns.map(str)

        means = df.pivot_table(columns='Host', index='Exitval',
                               values='JobRuntime', margins=True)
        means = means.reset_index()
        means.columns = means.columns.map(str)

        cuotas = df[df['Exitval'] == 0].pivot_table(index='Proyection',
                                                    columns='Paranoia',
                                                    values='seed',
                                                    aggfunc=len)
        cuotas = cuotas.reset_index()
        cuotas.columns = cuotas.columns.map(str)

        return df, counts, means, cuotas


df, counts, means, cuotas = load_df()
source = ColumnDataSource(df)
columns = [
        TableColumn(field="Host", title="Host", width=120),
        TableColumn(field="JobRuntime", title="Runtime",
                    formatter=NumberFormatter(format='00:00:00'), width=120),
        TableColumn(field="Exitval", title="Exit", width=50),
        TableColumn(field="EndTime", title="EndTime",
                    formatter=DateFormatter(format='dd/mm/yy'), width=160)
    ]
data_table = DataTable(source=source, columns=columns, fit_columns=True,
                       width=400, height=600, row_headers=False)

columns2 = [TableColumn(field=val, title=val) for val in counts.columns]
counts = ColumnDataSource(counts)
data_table2 = DataTable(source=counts, columns=columns2, fit_columns=True,
                        width=900, height=150, row_headers=False)

columns3 = [TableColumn(field='Exitval', title='ExitVal')]
columns3 += [TableColumn(field=val, title=val, formatter=NumberFormatter(format='00:00:00')) for val in means.columns[1:]]
means = ColumnDataSource(means)
data_table3 = DataTable(source=means, columns=columns3, fit_columns=True,
                        width=900, height=150, row_headers=False)

columns4 = [TableColumn(field=cuotas.columns[0], title=cuotas.columns[0])]
columns4.extend([TableColumn(field=val, title=val) for val in sorted(cuotas.columns[1:], key=int)])
coutas = ColumnDataSource(cuotas)
data_table4 = DataTable(source=coutas, columns=columns4, fit_columns=True,
                        width=450, height=400, row_headers=False)

curdoc().last_update = time.time()

ts1 = figure(x_axis_type='datetime', plot_height=300, plot_width=450)
ts1.xaxis.axis_label = 'Time'
ts1.yaxis.axis_label = 'Completed Jobs'
ts1_line = ts1.line('EndTime', 'Jobs Done', source=source)

ts2 = figure(x_axis_type='datetime', plot_height=300, plot_width=450)
ts2.xaxis.axis_label = 'Time'
ts2.yaxis.axis_label = 'Errors'
ts2_line = ts2.line('EndTime', 'Errors', source=source)
ts2.x_range = ts1.x_range


def update():
    """Load new data."""
    if os.path.getmtime('job-pipeline.tsv') > curdoc().last_update:
        temp, counts, means, cuotas = load_df()
        source.data = source.from_df(temp)
        data_table.source = source
        data_table2.source = ColumnDataSource(counts)
        data_table3.source = ColumnDataSource(means)
        data_table4.source = ColumnDataSource(cuotas)
    curdoc().last_update = time.time()


curdoc().add_periodic_callback(update, 5000)
c1 = column(widgetbox(data_table), width=500)
c2 = column(widgetbox(data_table2, data_table3),
            row(ts1, ts2),
            widgetbox(data_table4), width=900)
dash = row(c1, c2, width=1600, height=600)
curdoc().add_root(dash)
