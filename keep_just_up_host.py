import subprocess
import time
import smtplib
import filecmp
import pandas as pd
import os.path
import logging


format = ('%(filename)s: '
          '%(levelname)s: '
          '%(funcName)s(): '
          '%(lineno)d:\t'
          '%(message)s')
# FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(format=format, filename='keep_just_up_host.log',
                    level=logging.DEBUG)


def gmail(fromaddr, toaddr, msg):
    """Send mail."""
    filename = fromaddr.split('@')[0] + '_data.txt'
    with open(filename, 'r') as emaildata:
        data = emaildata.read().split('\n')
        username = data[0]
        password = data[1]
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.starttls()
    server.login(username, password)
    server.sendmail(fromaddr, toaddr, msg)
    server.quit()


class Host(object):
    """Host Class."""

    def __init__(self, name, cores, user, enter_time, exit_time, contact_type,
                 mail, home):
        """The name and user of the server.

        working_time is a tuple with the min and max time of working times
        during week days

        contact_type could be telegram, mail or none
        """
        self.name = name
        self.sshname = name
        if self.name == ':':
            self.sshname = 'localhost'
        self.user = user
        self.home = home
        self.mail = mail
        self.cores = cores
        self.contact_type = contact_type
        self.enter_time = enter_time
        self.exit_time = exit_time
        self.file_line = '/'.join([str(self.cores), self.name]) + '\n'
        self.msg_sent = False
        # working status: working or not working
        self.working_status = None
        # host status: up or down
        self.host_status = None
        # user status: logged in or logged out
        self.user_status = None
        # working hours status: working hors or r and r
        self.working_hours_status = None
        self.stop_flag_status = False
        self.start_flag_status = False
        self.paused_pids = []

    def is_up(self):
        """Test if server is up and stores the boolean result in self.is_up."""
        if self.name == ':':
            new_status = 'is up'
        else:
            command = ['ssh', '-o', 'ConnectTimeout=4']
            try:
                subprocess.check_output(command + [self.sshname, '"pwd"'])
                self.msg_sent = False
                new_status = 'is up'
            except subprocess.CalledProcessError:
                new_status = 'is down'
                # self.send_mesage()
        if self.host_status != new_status:
            logging.info(self.name + ' ' + new_status)
        if self.host_status=='is down' and new_status=='is up':
            logging.info(self.name + ' was down and now is up. update repos')
            branch = subprocess.check_output(['git', 'rev-parse', '--abbrev-ref', 'HEAD'])
            subprocess.call(['bash', 'push_nodes.sh', branch])
        self.host_status = new_status
        return self.host_status == 'is up'

    def send_mesage(self):
        """Contact the owner of the server."""
        msg = self.user + ', tu computadora esta apagada, no?'
        msg += 'la podes prender'
        if not self.msg_sent:
            if self.contact_type == 'telegram':
                subprocess.check_output(['telegram-send', msg])
            if self.contact_type == 'mail':
                gmail('***REMOVED***.com', self.mail, msg)
            self.msg_sent = True

    def should_work(self):
        """It Checks if the server should be working."""
        # if stop flag exist it should not work
        if self.stop_flag_exists():
            return self.update_staus('not_working')

        # if the host is my computer it should work
        cond = self.name == ':'
        # if start flag exist it should work
        cond = cond or self.start_flag_exists()
        # if is weekend it should work
        cond = cond or self.is_weekend()
        # if user is not logged in it should work
        cond = cond or not self.is_user_logged_in()
        # if it is at night it should work
        cond = cond or self.working_hour()
        if cond:
            return self.update_staus('working')
        else:
            return self.update_staus('not_working')

    def working_hour(self):
        """if it is at night it should work."""
        hour = time.localtime().tm_hour
        if self.enter_time < self.exit_time:
            if self.enter_time <= hour and hour < self.exit_time:
                new_status = 'in working hours'
            else:
                new_status = 'in r and r'
        else:
            if hour >= self.enter_time and hour < 24:
                new_status = 'in working hours'
            elif hour >= 0 and hour < self.exit_time:
                new_status = 'in working hours'
            else:
                new_status = 'in r and r'
        if new_status != self.working_hours_status:
            logging.info(self.name + ' is ' + new_status)
        self.working_hours_status = new_status
        return new_status == 'in working hours'

    def is_weekend(self):
        """check if is weekend."""
        day = time.localtime().tm_wday
        return day == 5 or day == 6

    def is_user_logged_in(self):
        """check if user is logged."""
        cmd = "who -q | head -1"
        try:
            out = subprocess.check_output(['ssh', self.sshname, cmd])
            if self.home in out:
                new_status = 'logged in'
            else:
                new_status = 'logged out'
        except subprocess.CalledProcessError as e:
            new_status = 'logged out'
            logging.warning(e)
        if new_status != self.user_status:
            logging.info(self.user + ' is ' + new_status + ' in ' + self.name)
        self.user_status = new_status
        return self.user_status == 'logged in'

    def kill_my_process(self):
        """kill my process."""
        logging.info('killing processes in ' + self.name)
        cmd = "ps ax  | grep  percolation.py | grep -v grep | grep R"
        try:
            out = subprocess.check_output(['ssh', self.sshname, cmd])
            pids = [x.strip().split(' ')[0] for x in out.split('\n')]
            pids = [pid for pid in pids if pid.isdigit()]
        except subprocess.CalledProcessError as e:
            logging.warning(e)
            pids = []
        self.paused_pids = pids
        for pid in pids:
            cmd = 'kill -TSTP ' + pid
            subprocess.call(['ssh', self.sshname, cmd])

    def reasum_process(self):
        """reasum my process."""
        for pid in self.paused_pids:
            cmd = 'kill -CONT ' + pid
            try:
                subprocess.call(['ssh', self.sshname, cmd])
            except subprocess.CalledProcessError as e:
                logging.warning(e)
        self.paused_pids = []

    def stop_flag_exists(self):
        """check if stop flag exists."""
        try:
            out = subprocess.check_output(['ssh', self.sshname, "ls /tmp/"])
            out = 'stop.andy' in out.split('\n')
            if out and not self.stop_flag_status:
                logging.info('Stop flag exists in ' + self.name)
        except subprocess.CalledProcessError as e:
            logging.warning(e)
            out = False
        self.stop_flag_status = out
        return out

    def start_flag_exists(self):
        """check if start flag exists."""
        out = subprocess.check_output(['ssh', self.sshname, "ls /tmp/"])
        out = 'start.andy' in out.split('\n')
        if out and not self.start_flag_status:
            logging.info('Start flag exists in ' + self.name)
        self.start_flag_status = out
        return out

    def __str__(self):
        """str rep."""
        return self.name

    def update_staus(self, new_status):
        """Update status.

        Status can be working, not_working or None. If it changes, it loggs the
        changes. If it changes from working to not working it runs
        kill_my_process.
        """
        if self.working_status == 'working' and new_status == 'not_working':
            self.kill_my_process()
        if self.working_status == 'not_working' and new_status == 'working':
            self.reasum_process()
        if self.working_status != new_status:
            logging.info(self.name + ' is ' + new_status)
        self.working_status = new_status
        return self.working_status == 'working'


df = pd.read_excel('nodes.xlsx')
hosts = list(df.apply(lambda x: Host(*x), axis=1))
mod_t = os.path.getmtime('nodes.xlsx')

while True:
    if os.path.getmtime('nodes.xlsx') != mod_t:
        df = pd.read_excel('nodes.xlsx')
        hosts = list(df.apply(lambda x: Host(*x), axis=1))
        mod_t = os.path.getmtime('nodes.xlsx')

    with open('nodes_new.txt', 'w') as f:
        for host in hosts:
            if host.is_up():
                if host.should_work():
                    f.write(host.file_line)
    if not filecmp.cmp('nodes.txt', 'nodes_new.txt'):
        print('change nodes')
        with open('nodes_new.txt', 'r') as f:
            print(f.read())
        subprocess.check_output(['cp', 'nodes_new.txt', 'nodes.txt'])
    time.sleep(10)
